# Cat-face-ds-compilation

## A compilation of various datasets of cat faces. 

- All pictures have been published online by their creators and are unlicensed or licensed under Creative Commons.
This means all pictures are open for redistribution, modification and personal usage.
See the `info.txt` of the various datasets for more details.

- Each of the folders contains files from a different dataset.

- None of the pictures have been modified.

### Used datasets:

- [animal-faces](https://www.kaggle.com/datasets/andrewmvd/animal-faces)

- [cat2dog](https://www.kaggle.com/datasets/waifuai/cat2dog)

- [cats-faces-64x64-for-generative-models](https://www.kaggle.com/datasets/spandan2/cats-faces-64x64-for-generative-models)

### Download

I won't provide releases or compressed archives of this compilation, so the best way to download it is simply with git:

```bash
git clone https://gitlab.com/gregorni/Cat-face-ds-compilation.git
```

### License

Since this repository doesn't contain any code or pictures of my own, I won't include a license. If you want to use this dataset compilation, the licenses of the various datasets apply.
